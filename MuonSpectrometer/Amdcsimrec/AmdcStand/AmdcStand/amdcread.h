/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef amdcread_H
#define amdcread_H
extern "C" void amdcreadnn_(char* FileName,int& SizeName,int& Istate,int& IFLAG,long FileName_len);
#endif
