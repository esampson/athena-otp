/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONREADOUTGEOMETRYR4_RPCREADOUTELEMENT_ICC
#define MUONREADOUTGEOMETRYR4_RPCREADOUTELEMENT_ICC


namespace ActsTrk{
    template <> inline Amg::Transform3D 
        TransformCacheDetEle<MuonGMR4::RpcReadoutElement>::fetchTransform(const DetectorAlignStore* store) const{
            return m_parent->toStation(store) * m_parent->fromGapToChamOrigin(hash());
        }
}

namespace MuonGMR4 {
    namespace RpcIdMeasHashFields{
        constexpr unsigned int minOne = -1;
        /// Bitwise layout of the measurement hash 
        ///         (   strip | meashPhi | doubletPhi [1-2]|  gasGap [1-3])
        ///                       1 bit         1 bit           
        constexpr unsigned int gasGapShift = 0;
        /// @brief gasGap ranges from 1-3 -> 2 bits
        constexpr unsigned int doubletPhiShift = 2;
        /// @brief doubletPhi ranges from 1-2 -> 1 bit
        constexpr unsigned int measPhiShift = doubletPhiShift + 1;
        /// @brief measPhi is one bit
        constexpr unsigned int channelShift = measPhiShift + 1;
    }
    inline double RpcReadoutElement::thickness() const { return 2.* m_pars.halfThickness; }
    inline double RpcReadoutElement::gasGapPitch() const {return m_gasThickness; }
    inline int RpcReadoutElement::doubletZ() const{ return m_doubletZ; }
    inline int RpcReadoutElement::doubletR() const{ return m_doubletR; }
    inline int RpcReadoutElement::doubletPhi() const{ return m_doubletPhi; }
    inline unsigned int RpcReadoutElement::nGasGaps() const { return m_pars.nGasGaps; }
    inline int RpcReadoutElement::nPhiPanels() const { return m_pars.nGapsInPhi; }
    inline int RpcReadoutElement::doubletPhiMax() const {return std::max(nPhiPanels(), doubletPhi()); }
    inline unsigned int RpcReadoutElement::nEtaStrips() const { return (m_pars.etaDesign ? m_pars.etaDesign->numStrips(): 0u); }
    inline unsigned int RpcReadoutElement::nPhiStrips() const { return (m_pars.phiDesign ? m_pars.phiDesign->numStrips(): 0u); }
    inline double RpcReadoutElement::stripEtaPitch() const { return (m_pars.etaDesign ? m_pars.etaDesign->stripPitch() : 0.); }
    inline double RpcReadoutElement::stripPhiPitch() const { return (m_pars.phiDesign ? m_pars.phiDesign->stripPitch() : 0.); }
    inline double RpcReadoutElement::stripEtaWidth() const { return (m_pars.etaDesign ? m_pars.etaDesign->stripWidth() : 0.); }
    inline double RpcReadoutElement::stripPhiWidth() const { return (m_pars.phiDesign ? m_pars.phiDesign->stripWidth() : 0.); }
    inline double RpcReadoutElement::stripEtaLength() const { return (m_pars.etaDesign ? m_pars.etaDesign->longHalfHeight()*2. : 0.);}
    inline double RpcReadoutElement::stripPhiLength() const { return (m_pars.phiDesign ? m_pars.phiDesign->longHalfHeight()*2. : 0.);}
    inline IdentifierHash RpcReadoutElement::measurementHash(const Identifier& measId) const {
        if (m_idHelper.doubletZ(measId) != doubletZ() ||
            (doubletPhi() != 1 && m_idHelper.doubletPhi(measId) == 1) || 
            m_idHelper.elementID(measId) != m_idHelper.elementID(identify()) ) {
            ATH_MSG_WARNING("The measurement " << idHelperSvc()->toString(measId)
                            << " picks the wrong readout element " << idHelperSvc()->toStringDetEl(identify()));
        }
        return createHash(m_idHelper.strip(measId),
                          m_idHelper.gasGap(measId),
                          m_idHelper.doubletPhi(measId),
                          m_idHelper.measuresPhi(measId));
    }
    inline IdentifierHash RpcReadoutElement::layerHash(const Identifier& measId) const {
        if (m_idHelper.doubletZ(measId) != doubletZ() ||
            (doubletPhi() != 1 && m_idHelper.doubletPhi(measId) == 1) || 
            m_idHelper.elementID(measId) != m_idHelper.elementID(identify()) ) {
            ATH_MSG_WARNING("The measurement " << idHelperSvc()->toString(measId)
                            << " picks the wrong readout element " << idHelperSvc()->toStringDetEl(identify()));
        }
        return createHash(0, m_idHelper.gasGap(measId),
                             m_idHelper.doubletPhi(measId),
                             m_idHelper.measuresPhi(measId));
    }
inline IdentifierHash RpcReadoutElement::layerHash(const IdentifierHash& measHash) const {
    using namespace RpcIdMeasHashFields;
    constexpr unsigned int mask = minOne << channelShift;
    return IdentifierHash{static_cast<unsigned int>(measHash) & (~mask)};
}
inline IdentifierHash RpcReadoutElement::createHash(const unsigned int strip, 
                                                    const unsigned int gasGap, 
                                                    const unsigned int doubPhi, 
                                                    const bool measPhi) {
    using namespace RpcIdMeasHashFields;
    const IdentifierHash hash{ strip << channelShift | measPhi << measPhiShift |
                               (doubPhi -1) << doubletPhiShift | (gasGap -1) << gasGapShift };
    return hash;
}
inline unsigned int RpcReadoutElement::stripNumber(const IdentifierHash& measHash) {
    using namespace RpcIdMeasHashFields;
    return static_cast<unsigned int>(measHash) >> channelShift;
}
inline unsigned int RpcReadoutElement::gasGapNumber(const IdentifierHash& measHash) {
    using namespace RpcIdMeasHashFields;
    constexpr unsigned int mask = minOne << doubletPhiShift;
    const unsigned int stripedHash = (~mask) & static_cast<unsigned int>(measHash);
    return ( stripedHash >> gasGapShift);
}
inline unsigned int RpcReadoutElement::doubletPhiNumber(const IdentifierHash& measHash) {
    using namespace RpcIdMeasHashFields;
    constexpr unsigned int mask = minOne << measPhiShift;
    const unsigned int stripedMask = (~mask) & static_cast<unsigned int>(measHash);
    return (stripedMask >> doubletPhiShift);
}
inline bool RpcReadoutElement::measuresPhi(const IdentifierHash& measHash) {
    using namespace RpcIdMeasHashFields;
    constexpr unsigned int mask = minOne << channelShift;
    const unsigned int stripedMask = (~mask) & static_cast<unsigned int>(measHash);    
    return (stripedMask >> measPhiShift);
}
inline const StripLayer& RpcReadoutElement::sensorLayout(const IdentifierHash& measHash) const {
    const IdentifierHash layHash = layerHash(measHash);
    const unsigned int layIdx{static_cast<unsigned int>(layHash)};
    if (layIdx >= m_pars.layers.size() || !m_pars.layers[layIdx]) {
        ATH_MSG_FATAL("The sensorLayout does not exist for "<<idHelperSvc()->toStringDetEl(identify())
                      <<" gasGap "<<(gasGapNumber(measHash) +1)
                      <<" doubletPhi: "<<(doubletPhiNumber(measHash) + 1)
                      <<" isPhiChannel: "<<measuresPhi(measHash)
                      <<" strip: "<<stripNumber(measHash) );
        throw std::runtime_error("Invalid sensor layout");
    }
    return (*m_pars.layers[layIdx]);
}

inline Identifier RpcReadoutElement::measurementId(const IdentifierHash& measHash) const {
    return m_idHelper.channelID(identify(), doubletZ(), doubletPhiNumber(measHash) + 1, 
                                gasGapNumber(measHash) + 1, measuresPhi(measHash), stripNumber(measHash));  
}

inline Amg::Vector3D RpcReadoutElement::stripPosition(const ActsGeometryContext& ctx, const Identifier& measId) const {
    return stripPosition(ctx, measurementHash(measId));
}  
inline Amg::Vector3D RpcReadoutElement::rightStripEdge(const ActsGeometryContext& ctx, const Identifier& measId) const{
    return rightStripEdge(ctx, measurementHash(measId));
}
inline Amg::Vector3D RpcReadoutElement::leftStripEdge(const ActsGeometryContext& ctx, const Identifier& measId) const{
    return leftStripEdge(ctx, measurementHash(measId));
}
}  // namespace MuonGMR4
#endif
